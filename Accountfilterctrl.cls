public with sharing class Accountfilterctrl {  
	
	
    public static map<id,Account> stu {set;get;}
  
    public static set<id> stuids {set;get;}
    public static set<string> stuattids {set;get;}
	//added by santosh 
    public static set<string> classids {set;get;}
    //testing for conflict
    public Accountfilterctrl(){
        
        
    }
	
	
	@AuraEnabled
    public static list<account> getAccounts(string statevalue){
        system.debug('calling in');
        stu =new map<id,Account>( [select id,name,billingstate,Phone,Type from account where billingstate = :statevalue]);
        //system.debug(stu.id);
        stuids = new set<id>(); 
        stuids = stu.keyset(); 
        system.debug('calling in@@@@@@@@@@@@@@@@'+stu.values());  
        return stu.values();
    }   
    
}